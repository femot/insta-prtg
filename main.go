package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"

	"github.com/PaesslerAG/go-prtg-sensor-api"
	"github.com/ahmdrz/goinsta"
)

func main() {
	user, password := flags()
	insta, err := login(user, password)
	if err != nil {
		log.Fatal(err)
	}
	defer insta.Logout()

	fmt.Println(buildResponse(insta).String())
}

func buildResponse(insta *goinsta.Instagram) *prtg.SensorResponse {
	response := new(prtg.SensorResponse)

	followers, following, err := followerStats(insta)
	if err != nil {
		response.PRTG.Error = err.Error()
		return response
	}

	response.AddChannel(prtg.SensorChannel{
		Channel:   "Followers",
		Value:     strconv.Itoa(followers),
		ShowChart: 1,
		ShowTable: 1,
	})
	response.AddChannel(prtg.SensorChannel{
		Channel:   "Following",
		Value:     strconv.Itoa(following),
		ShowChart: 1,
		ShowTable: 1,
	})

	return response
}

func test(insta *goinsta.Instagram) error {
	fmt.Printf("Full name is: %s\n", insta.LoggedInUser.FullName)
	fers, fing, err := followerStats(insta)
	if err != nil {
		return fmt.Errorf("failed to get follower stats: %v", err)
	}
	fmt.Printf("Following: %d\nFollowers: %d\n", fing, fers)
	return nil
}

func followerStats(insta *goinsta.Instagram) (followers, following int, err error) {
	resp, err := insta.TotalUserFollowers(insta.LoggedInUser.ID)
	if err != nil {
		return 0, 0, fmt.Errorf("failed to get followers: %v", err)
	}
	followers = len(resp.Users)

	resp, err = insta.TotalUserFollowing(insta.LoggedInUser.ID)
	if err != nil {
		return 0, 0, fmt.Errorf("failed to get following: %v", err)
	}
	following = len(resp.Users)
	return
}

func login(user, password string) (*goinsta.Instagram, error) {
	insta := goinsta.New(user, password)
	err := insta.Login()
	if err != nil {
		return nil, fmt.Errorf("failed to login: %v", err)
	}
	return insta, nil
}

func flags() (user, password string) {
	flag.StringVar(&user, "u", "", "Enter username")
	flag.StringVar(&password, "p", "", "Enter password")
	flag.Parse()
	return
}
