[![build status](https://gitlab.com/femot/insta-prtg/badges/master/build.svg)](https://gitlab.com/femot/insta-prtg/commits/master)
[![coverage report](https://gitlab.com/femot/insta-prtg/badges/master/coverage.svg)](https://gitlab.com/femot/insta-prtg/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/femot/insta-prtg?status.svg)](http://godoc.org/gitlab.com/femot/insta-prtg)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/femot/insta-prtg)](https://goreportcard.com/report/gitlab.com/femot/insta-prtg)

# Instagram Sensor for PRTG
This is just a fun sensor, which is inspired by this Youtube video

[![PRTG Wedding](http://img.youtube.com/vi/u6SPukMJNSA/0.jpg)](http://www.youtube.com/watch?v=u6SPukMJNSAE "Video Title")

There probably is no "real" monitoring value in this, but I don't care :stuck_out_tongue:

# Usage
You need to call it with the following parameters:
```
-u username -p password
```

Enter those in the `Parameters` field when adding the sensor and replace
username/password with your credentials.

More information about custom sensors: [PRTG Manual: EXE/Script Advanced Sensor](https://www.paessler.com/manuals/prtg/exe_script_advanced_sensor)

# Download
[Latest version for Windows-64bit](https://gitlab.com/femot/insta-prtg/builds/artifacts/master/file/artifacts/insta-prtg.exe?job=compile)